interface:

inputs:
periph_clk -> local (ref) clock, at least 2x spi_clk, may be difficult to making timing, suggest more room - chiefly due to the data_ready flag
spi_clk -> exposed spi clock/sclk
_periph_rst -> resets the SPI peripheral entirely, synchronous with periph_clk
_spi_ss -> exposed spi slave-select, spi_so is set to hi-Z when deasserted
spi_si -> spi serial input (MOSI)
d_in -> data to write out when the master clocks us. latched both while idle and on the last byte (for fast reads)

outputs:
data_good -> high while d_out contains valid data. lasts at most one spi_clk period, offset negatively by up to one half periph_clk period.
d_out -> contents of the input buffer, is not guaranteed to contain properly formatted data after one periph_clk cycle
spi_so -> spi serial output (MISO)


design notes:
built with CDC integrated - nothing is clocked off of the SPI clock domain, instead edges are detected and acted upon. This -should- still allow for a useful peripheral at SPI speeds up to 0.5x the internal clock, but care will be necessary in handling the recieived data. Probably you cannot operate on it fast enough anyway. So much for full duplex :)
