`timescale 1ns/1ns

module spi_tb;

  reg reset = 0;
  reg clock = 0;
  reg spi_clock = 0;
  reg SS = 1;
  reg MOSI = 0;
  wire MISO;
  wire rdy;

  reg [7:0] spi_in_byte;
  wire [7:0] spi_out_byte;


  	// 100mhz
  	always #5 clock = !clock;
    // half for spi clock from master

  `define t 10 // spi clock period
  `define bit(val) MOSI = val; #`t spi_clock <= 1; #`t spi_clock <= 0;


  spi_slave s(
    .periph_clk(clock),
    .spi_clk(spi_clock),
    ._periph_rst(reset),

    ._spi_ss(SS),
    .spi_si(MOSI),
    .spi_so(MISO),

    .data_good(rdy),

    .d_out(spi_out_byte),
    .d_in(spi_in_byte)
    );

  initial begin
    $dumpfile("spitest.vcd");
    $dumpvars(0, s);

    #0 reset = 0; spi_in_byte = 8'h5A;
    #10 reset = 1;
    #10 SS = 0;
    `bit(0); `bit(1); `bit(0); `bit(1); `bit(0); `bit(1); `bit(0); `bit(1);
    #10 SS = 1;
    `bit(0); `bit(1); `bit(0); `bit(1); `bit(0); `bit(1); `bit(0); `bit(1);
    #10 SS = 0;
    `bit(1); `bit(0); `bit(0); `bit(1); `bit(0); `bit(1); `bit(1); `bit(0);
    `bit(0); `bit(0); `bit(0); `bit(0); `bit(0); `bit(0); `bit(0); `bit(0);
    `bit(1); `bit(0); `bit(0); `bit(1); `bit(0); `bit(1); `bit(1); `bit(0);
    #20 $finish;
  end
endmodule
