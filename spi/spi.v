/*

NOTE: currently written to target SPI mode 0,0 / mode 0:
- clock idles low
- data lines are driven by the falling edge, sampled on the rising
- serial out doesn't behave well when driven with an uneven clock, unless the master samples on falling edge

SI interface:
  out:
    data ready flag
    data out[7:0]
  in:
    ack - marks data out as stale/consumed
    clk - system clock. must be >= 2x as fast as spi_clk
    spi_clk - spi clock. must be <= 0.5x as fast as spi_clk
    spi_si - spi serial input. registered on

SO interface:
  out:
    spi_so - spi serial output, synchronized to spi_clk
  in:
    data in[7:0] - data output buffer to serialize
    clk - system clock. must be >= 2x as fast as spi_clk
    spi_clk - spi clock. must be <= 0.5x as fast as spi_clk
*/

module spi_slave(
  // common interface
  input wire periph_clk,  // local clock
  input wire spi_clk, // SPI clock
  input wire _periph_rst, // peripheral reset, active low
  // input interface
  input wire _spi_ss,  // slave select, active low
  input wire spi_si,  // MOSI
  output wire data_good,  // data ready strobe
  output reg [7:0] d_out, // last read byte
  // output interface
  input wire [7:0] d_in, // data to write out
  output reg spi_so  // MISO
  );

  // "working" data output register - shift register is much cheaper than "indexing" into a FF array...
  reg [7:0] d_in_r = 2'hff;

  reg [2:0] cur_bit = 0;
  // edge detector registers
  reg [1:0] last_clk = 2'b00;
  // registered input:
  reg spi_si_r = 0;

  assign data_good = (cur_bit == 0);

  always @(*) begin
    if(_spi_ss == 1'b0) spi_so = d_in_r[7];
    else spi_so = 1'bz;
  end

  always @(posedge periph_clk) begin
    if(!_periph_rst) begin
      spi_si_r <= 0;
      last_clk <= 0;
      d_out <= 0;
      cur_bit <= 0;
      d_in_r <= d_in;
    end
    else begin
      // edge detection:
      last_clk <= {last_clk[0], spi_clk};

      // always have an input sample on hand:
      spi_si_r <= spi_si;

      if(_spi_ss == 1'b0) begin
        if(last_clk == 2'b01) begin
          // next bit transaction
          if(cur_bit == 3'h0) d_in_r <= d_in;
          cur_bit <= cur_bit + 1;
          d_out <= {d_out[6:0], spi_si_r};
        end
        if(last_clk == 2'b10) begin
          if(cur_bit != 3'h0) d_in_r <= {d_in_r[6:0], 1'b1};
        end
      end else begin
        // pretty sure we cannot be deselected mid-transfer
        cur_bit <= 3'h0;
      end
    end
  end
endmodule
