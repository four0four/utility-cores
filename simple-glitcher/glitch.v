/*

interface:
  inputs:
    clock - sysclock on which all configured parameters are based
    _reset - total reset, synchronous with clock. low to reset, high to run.
    arm - reset counters to intial values + enable, loading the following parameters:
    preload - preload value for counter
    begin_glitch - initial compare point, assert glitch output
    end_glitch - at which to deassert glitch output

  outputs:
    glitch - duh
    triggered - latching version of the above

  parameters:
    BITS - number of bits in the glitcher's counters/compares

  note: glitch can be at minimum one clock cycle wide - fancier stuff can be done using tech-specific hard IP (delay lines!) if necessary

*/

module simple_glitcher #(
  parameter BITS = 32
  ) (
  input wire clock,
  input wire _reset,
  input wire arm,
  input wire [BITS-1:0] preload,
  input wire [BITS-1:0] start_glitch,
  input wire [BITS-1:0] end_glitch,
  output reg glitch, triggered
  );

  reg [BITS-1:0] counter_r = 0;
  reg [BITS-1:0] end_glitch_r = 0;
  reg [BITS-1:0] start_glitch_r = 0;

  reg armed = 0;
	reg arm_shreg = 2'b00;

  always @(posedge clock) begin
    if(_reset == 1'b0) begin
      glitch <= 1'b0;
      counter_r <= 0;
      end_glitch_r <= 0;
      start_glitch_r <= 0;
      armed <= 1'b0;
      triggered <= 0;
			arm_shreg <= 2'b00;
    end else begin
			arm_shreg <= {arm_shreg[0], arm};
      if(armed == 1'b1) begin
        counter_r <= counter_r + 1'b1;
        if(counter_r == start_glitch_r) begin
          glitch <= 1'b1;
          triggered <= 1'b1;
        end else if(counter_r == end_glitch_r) begin
          glitch <= 1'b0;
          armed <= 1'b0;
        end
      end else if(arm_shreg == 2'b01) begin
        counter_r <= preload;
        end_glitch_r <= end_glitch;
        start_glitch_r <= start_glitch;
        armed <= 1'b1;
        triggered <= 1'b0;
      end
    end
  end
endmodule
