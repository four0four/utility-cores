`timescale 1ns/1ns

module uart_tb;

	reg reset=0;
	reg clock=0;
	reg rx=1;
	wire tx;
	wire rx_done;
	wire txing;
	wire [7:0] d_out;
	reg go = 0;
	reg [7:0] txbuf = 8'h41;

	initial begin
		$dumpfile("test.vcd");
		$dumpvars(0, u);
		#0 reset = 0;
		#20 reset = 1;
		#0 go = 1;
		// start bit
		#8680 rx = 0;
		#0 go = 1;
		// bit 0
		#8680 rx = 0;
		// bit 1
		#8680 rx = 1;
		// bit 2
		#8680 rx = 0;
		// bit 3
		#8680 rx = 1;
		// bit 4
		#8680 rx = 0;
		// bit 5
		#8680 rx = 1;	
		// bit 6
		#8680 rx = 0;
		// bit 7
		#8680 rx = 1;
		// stop bit
		#0 go = 0;
		#8680 rx = 1;
		#100000 $finish;
	end

	// 100mhz
	always #5 clock = !clock;

	uart u(
		.clk(clock),
		.div(8'd53),
		.rx(rx),
		.d_in(txbuf),
		._reset(reset),
		.d_out(d_out),
		.rx_done(rx_done),
		.tx_busy(txing),
		.tx(tx),
		.tx_en(go)
	);
endmodule
