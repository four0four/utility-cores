// outputs 50% duty cycle "clock"
module uart_clk_div #(
	parameter DIV_BITS = 8,
	parameter RST_POL = 0
	) (
	input wire clk_in,
	output reg clk_out,
	input wire _reset,
	input wire [DIV_BITS-1:0] div);

	reg [DIV_BITS-1:0] clk_div = 0;

	always @(posedge clk_in) begin
		if(_reset == 1'b0) begin
			clk_div <= div;
			clk_out <= RST_POL;
		end else begin
			clk_div <= clk_div - 1;
			if(clk_div == 0) begin
				clk_div <= div;
				clk_out <= ~clk_out;
			end
		end
	end

endmodule

// calculate divider: [baud rate] = (clk/div)/8, div = clk / (baud * 8) - 1 (or something)
// 115200 baud @ 110mhz -> 107.5
// 115200 baud @ 20mhz -> 20.7
// 115200 baud @ 16mhz -> 16.4
module uart(
	input wire clk, input wire [7:0] div, input wire rx, input wire [7:0] d_in, input wire _reset, input wire tx_en,
	output wire [7:0] d_out, output wire rx_done, output wire tx_busy, output reg tx);

	parameter STATE_IDLE = 0;
	parameter STATE_TRANSFER = 1;
	parameter STATE_START = 2;
	parameter STATE_STOP = 3;

	// our position within the bit
	reg [2:0] rx_bit_pos;
	reg [2:0] tx_bit_pos;

	// little phase offset here to account for imperfect bauds
	wire take_sample = (rx_bit_pos == 3'h2);

	// states
	reg [1:0] rx_state;
	reg [1:0] tx_state;

	// rx buffers
	reg rx_bit;
	reg [7:0] rx_buf;
	reg r_rx_done;
	assign rx_done = r_rx_done;
	// actual bit indices
	reg [2:0] rx_nbit;
	reg [2:0] tx_nbit;

	assign d_out = rx_buf;
	assign tx_busy = (tx_state != STATE_IDLE);


	// baud rate gen stuffs
	wire tx_clk_div, rx_clk_div;
	reg tx_clk_rst, rx_clk_rst;

	uart_clk_div tx_divider(
		.clk_in(clk),
		.clk_out(tx_clk_div),
		._reset(_reset),
		.div(div)
	);

	uart_clk_div rx_divider(
		.clk_in(clk),
		.clk_out(rx_clk_div),
		._reset(_reset),
		.div(div)
	);

	// tx state machine
	always@(posedge tx_clk_div or negedge _reset) begin
		if(_reset == 1'b0) begin
			tx_bit_pos <= 3'b0;
			tx_state <= STATE_IDLE;
			tx <= 1;
			tx_nbit <= 0;
			//tx_clk_rst <= 0;
		end
		else /*if(tx_clk_div)*/ begin
			//tx_clk_rst <= 0;
			case(tx_state)
				STATE_IDLE: begin
					tx_bit_pos <= 0;
					tx_nbit <= 0;
					tx <= 1;
					if(tx_en == 1'b1) begin
						tx_state <= STATE_START;
					end
				end
				STATE_START: begin
					tx <= 1'b0;
					tx_bit_pos <= tx_bit_pos + 1'b1;
					if(tx_bit_pos == 3'h7) begin
						tx_state <= STATE_TRANSFER;
						tx_bit_pos <= 3'b0;
					end
				end
				STATE_TRANSFER: begin
					tx <= d_in[tx_nbit];
					tx_bit_pos <= tx_bit_pos + 1'b1;
					if(tx_bit_pos == 3'h7) begin
						if(tx_nbit == 3'h7) begin
							tx_bit_pos <= 3'b0;
							tx_nbit <= 3'b0;
							tx_state <= STATE_STOP;
						end else begin
							tx_nbit <= tx_nbit + 1'b1;
							tx_bit_pos <= 3'b0;
						end
					end
				end
				STATE_STOP: begin
					tx <= 1'b1;
					tx_bit_pos <= tx_bit_pos + 1'b1;
					if(tx_bit_pos == 3'h7) begin
						tx_state <= STATE_IDLE;
						tx_bit_pos <= 3'b0;
					end
				end
			endcase
		end /*else begin
			tx_clk_rst <= 1;
		end*/
	end

	// rx state machine
	always@(posedge rx_clk_div or negedge _reset) begin
		if(_reset == 1'b0) begin
			rx_buf <= 8'b0;
			rx_bit_pos <= 3'b0;
			rx_bit <= 1'b0;
			rx_nbit <= 0;
			r_rx_done <= 0;
			rx_state <= STATE_IDLE;
		//	rx_clk_rst <= 0;
		end
		else /*if(rx_clk_div)*/ begin
		//	rx_clk_rst <= 0;
			case(rx_state)
				STATE_IDLE: begin
					// wait for falling edge
					if(rx == 1'b0) begin
						rx_state <= STATE_START;
						rx_bit_pos <= rx_bit_pos + 1;
					end

				end
				STATE_START: begin
					// wait 8 cycles and sample in the middle
					// progress if that was actually a start bit
					rx_bit_pos <= rx_bit_pos + 1;
					if(take_sample) rx_bit <= rx;
					if(rx_bit_pos == 3'h7) begin
						if(rx_bit == 1'b0) begin
							rx_state <= STATE_TRANSFER;
							rx_bit_pos <= 3'h0;
							rx_nbit <= 0;
						end else begin
						//	$display("UART WTF: got runt start bit?");
							rx_state <= STATE_IDLE;
							rx_bit_pos <= 3'h0;
						end
					end
				end
				STATE_TRANSFER: begin
					rx_bit_pos <= rx_bit_pos + 1;
					if(take_sample)  rx_buf <= {rx, rx_buf[7:1]};
					if(rx_bit_pos == 3'h7) begin
						rx_bit_pos <= 0;
						rx_nbit <= rx_nbit + 1;
					end
					if(rx_nbit == 3'h7 && (rx_bit_pos == 3'h7)) begin
						rx_nbit <= 0;
						rx_state <= STATE_STOP;
						rx_bit_pos <= 0;
					end
				end
				STATE_STOP: begin
					// this isn't great, but we've got the whole byte by now...
					rx_bit_pos <= rx_bit_pos + 1;
					r_rx_done <= 1;
					if(take_sample) rx_bit <= rx;
					if(rx_bit_pos == 3'h7) begin
						rx_state <= STATE_IDLE;
						rx_buf <= 8'h00;
						r_rx_done <= 0;
						$display("UART: got 0x%02x", rx_buf);
						if(rx_bit != 1'b1)
							$display("UART WTF: stop bit not set!");
					end
				end
			endcase
		end /*else begin
			rx_clk_rst <= 1;
		end */
	end

endmodule // uart
